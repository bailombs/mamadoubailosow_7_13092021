// Fonction filtre avec methodes d'array
export const search1 = (arrayRecipes, contentSearch) => {

    return arrayRecipes.filter(
        (recipe)=> {               
        return recipe.name.toLowerCase().includes(contentSearch) ||
            recipe.description.toLowerCase().includes(contentSearch) ||
            recipe.ingredients.some((ingredient) => {
                return ingredient.ingredient.toLowerCase().includes(contentSearch)
            }) ;               
        }
    )
}



// Fonction filtre avec boucle for
export const search2 = (arrayParam, contentSearch) => {
    const recipesBySearch = [];
   
    for(let recipe of arrayParam) {  
        console.log(recipe, ingredientFiltered(recipe, contentSearch));                    
        if(recipe.name.toLowerCase().includes(contentSearch) ||
            recipe.description.toLowerCase().includes(contentSearch) ||
            ingredientFiltered(recipe, contentSearch)) {           
            recipesBySearch.push(recipe)                                   
        } 
    }       
    return recipesBySearch;     
}



const ingredientFiltered = (recipe, contentSearch) => { 

    for( let ingredient of recipe.ingredients) {
        if(ingredient.ingredient.toLowerCase().includes(contentSearch)){
            return true
        }        
    }   
    return false;             
}


export const  dropdownMenuFilter = (filteredMenu, listText)=> {

    return filteredMenu.filter(
        (recipe) => { 
        return recipe.appliance.toLowerCase().includes(listText.toLowerCase()) ||
        recipe.ustensils.some((ustensil)=> {
            return ustensil.toLowerCase().includes(listText.toLowerCase()) 
        }) ||
        recipe.ingredients.some((ingredient)=> {
            return ingredient.ingredient.toLowerCase().includes(listText.toLowerCase());
        })
    })
}
