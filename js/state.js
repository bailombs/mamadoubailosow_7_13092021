import { recipes } from "./recipes.js";

const state = {
    filterRecipes : recipes,
}

export  const getFilterrecipes = () => {
    return state.filterRecipes;
}

export  const setFilterrecipes = (recipes) => {
    state.filterRecipes = recipes;
}


// Menu deroulant state
const dropDownStateDevices = {
    filterDropDownMenus : [],
}

// state  appareils
export const getDropDownFiltersDevices = () => {
    return dropDownStateDevices.filterDropDownMenus;
}

export const setDropDownFiltersDevices = (dropDownLists) => {
    dropDownStateDevices.filterDropDownMenus = dropDownLists;
}


// state ingredients
const dropDownStateIngredients = {
    filterDropDownMenus : [],
}

export const getDropDownFiltersIngredients = () => {
    return dropDownStateIngredients.filterDropDownMenus;
}

export const setDropDownFiltersIngredients = (dropDownLists) => {
    dropDownStateIngredients.filterDropDownMenus = dropDownLists;
}

// state ustensiles
const dropDownStateUtensils = {
    filterDropDownMenus : [],
}

export const getDropDownFiltersUtensils = () => {
    return dropDownStateUtensils.filterDropDownMenus;
}

export const setDropDownFiltersUtensils = (dropDownLists) => {
    dropDownStateUtensils.filterDropDownMenus = dropDownLists;
}