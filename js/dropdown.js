import { primaryColor, secondaryColor, tertiaryColor } from "./color.js";
import { displayRecipes } from "./recipe.js";
import { dropdownMenuFilter } from "./search.js";
import { getDropDownFiltersDevices, getDropDownFiltersIngredients, getDropDownFiltersUtensils, getFilterrecipes, setFilterrecipes } from "./state.js";
import { createTags } from "./tags.js";
import { displayUpDateDropDownItems } from "./updateDropdown.js";

export const dropdownMenus = (containerMenu, btnGroupToggle, icon, dropdownMenu, menuDropdownItems, nameMenu, type) => {

    btnGroupToggle.addEventListener('click', () => {
        icon.classList.toggle('rotate');    
        dropdownMenu.classList.toggle('hide');
        if(dropdownMenu.classList.contains('hide')){
            btnGroupToggle.setAttribute("aria-expanded", "false");
            btnGroupToggle.setAttribute("type","button");
            btnGroupToggle.setAttribute("value", nameMenu);
            btnGroupToggle.removeAttribute("placeholder")
            containerMenu.style.borderBottomLeftRadius ="5px"; 
            containerMenu.style.borderBottomRightRadius ="5px";
                
        } else {
            btnGroupToggle.setAttribute("aria-expanded", "true"); 
            btnGroupToggle.setAttribute("type","text");
            btnGroupToggle.setAttribute("placeholder",getSearchTitle(type));
            btnGroupToggle.removeAttribute("value");
            containerMenu.style.borderBottomLeftRadius ="0px"; 
            containerMenu.style.borderBottomRightRadius ="0px";             
        }       
    })
   
    
    //Appel de la fonction  creation de la liste des menus
    menuDropdownItems;
    
   
    // recuperation des li de chaque menu
    const listMenus = dropdownMenu.querySelectorAll('li');
    const tagsMenu = document.querySelector('.menus__tags');
    console.log(dropdownMenu);
    for( let listMenu of listMenus) {                   
        listMenu.addEventListener('click', ()=> {       
            const listText = listMenu.textContent;
            btnGroupToggle.innerHTML = listText;
            dropdownMenu.classList.toggle('hide'); 
            displayMenu(containerMenu, dropdownMenu, listMenus) ; 
            btnGroupToggle.setAttribute("type","button");
            btnGroupToggle.setAttribute("value", nameMenu);
            containerMenu.style.borderBottomLeftRadius ="5px"; 
            containerMenu.style.borderBottomRightRadius ="5px";
            const filteredRecipes = getFilterrecipes()    
            createTags(tagsMenu, listText, getColor(type), dropdownMenu, listMenu, filteredRecipes);
            removeItemMenu(listMenu, tagsMenu, dropdownMenu); 

            
          
            
            console.log(filteredRecipes);
           
            const newFilterRecipes =  dropdownMenuFilter(filteredRecipes, listText);
            console.log(newFilterRecipes);           
            setFilterrecipes(newFilterRecipes)   
            const getNewFilteredRecipes = getFilterrecipes()         
            const containerRecipes = document.querySelector('.container-recipes');
           
            console.log(getNewFilteredRecipes);

            const ingredientsInitial = newIngredientDropDownFiltered(filteredRecipes)
            const devicesInitial = newDeviceDropDownFiltered(filteredRecipes);  
            const utensilsInitial = newUtensilDropDownFiltered(filteredRecipes);  

            
            let recoverDropdownIngredientsFiltered = getDropDownFiltersIngredients();
            let recoverDropdownDevicesFiltered = getDropDownFiltersDevices();
            let recoverDropdownUtensilsFiltered = getDropDownFiltersUtensils();
            
                      
            containerRecipes.innerHTML = ""
            displayRecipes(containerRecipes, getNewFilteredRecipes);  
            
           
           
            
            recoverDropdownIngredientsFiltered = ingredientsInitial;
            recoverDropdownDevicesFiltered = devicesInitial;
            recoverDropdownUtensilsFiltered = utensilsInitial;
            

            console.log(recoverDropdownIngredientsFiltered);
                        
            const newIngredientsFiltered = newIngredientDropDownFiltered(getNewFilteredRecipes);
            const newDevicesFiltered = newDeviceDropDownFiltered(getNewFilteredRecipes);  
            const newUtensilsFiltered = newUtensilDropDownFiltered(getNewFilteredRecipes);  

            console.log(newIngredientsFiltered, newDevicesFiltered, newUtensilsFiltered);

            const finalIngredientsDropDownFiltered =  ArraysComparison(recoverDropdownIngredientsFiltered, newIngredientsFiltered)
            const finalDevicesDropDownFiltered =  ArraysComparison(recoverDropdownDevicesFiltered, newDevicesFiltered)
            const finalUtensilsDropDownFiltered =  ArraysComparison(recoverDropdownUtensilsFiltered, newUtensilsFiltered)

            console.log(finalIngredientsDropDownFiltered, finalDevicesDropDownFiltered, finalUtensilsDropDownFiltered);
            
            const listMenusIngredients = document.querySelectorAll('.ingredient__dropdownmenu--item'); 
            const listMenusDevices = document.querySelectorAll('.device__dropdownmenu--item'); 
            const listMenusUtensils = document.querySelectorAll('.utensil__dropdownmenu--item'); 

            displayUpDateDropDownItems(listMenusIngredients, finalIngredientsDropDownFiltered);
            displayUpDateDropDownItems(listMenusDevices, finalDevicesDropDownFiltered)  ;              
            displayUpDateDropDownItems(listMenusUtensils, finalUtensilsDropDownFiltered)
                                                                            
                                  
        })
    }

    // evenement listener du click sur le menu
    btnGroupToggle.addEventListener('click', ()=>{               
        //appel fonction pour le changement d'affichage du menu deroulant au click
        displayMenu(containerMenu, dropdownMenu, listMenus)
    })
    

       
    // Evemenent listener de saisi dans le menu deroulant 
    btnGroupToggle.addEventListener('input', () => {        
        dislayDropDownMenuFiltered(listMenus, btnGroupToggle);
    })        
}

// fonction changement d'affichage du menu  deroulant au click
export const displayMenu = (container, dropdrown, listmenus) => {
    for(let listmenu of listmenus) {
        if(dropdrown.classList.contains('hide')) {
            container.style.width = "180px";
            listmenu.style.width ="auto";            
        } else {
            container.style.width ="auto";
            listmenu.style.width ="120px";           
        }
    }

}   


export const dislayDropDownMenuFiltered =(listItems, btnGroupToggle)=> {

    let contentBtnGroupToggle = btnGroupToggle.value.toLowerCase();        
    for(let listMenu of listItems) {          
        if(listMenu.textContent.toLowerCase().includes(contentBtnGroupToggle)) {
            listMenu.style.display = "block";
        } else {
            listMenu.style.display = "none";
        }
    }           
}


//fonction supprimer tag  du menu deroulant
const  removeItemMenu = (listItemMenu, tagsMenu, dropdownMenu) => {
    const allTags = tagsMenu.querySelectorAll('.menus__tags--tagcontainer')
    for(let tag of allTags) {
        if(listItemMenu.textContent === tag.children[0].value){
            dropdownMenu.removeChild(listItemMenu)
        }
    }
}

const  getSearchTitle = (type) => {

    switch (type) {
        case 'Ingredient':            
            return 'Rechercher un ingredient';
       
        case 'Appareil':            
            return 'Rechercher un appareil';       
        case 'Ustensile':            
            return 'Rechercher un ustensile';
          
    }
}
const  getColor = (type) => {

    switch (type) {
        case 'Ingredient':            
            return  primaryColor;
       
        case 'Appareil':            
            return secondaryColor;       
        case 'Ustensile':            
            return  tertiaryColor;
          
    }
}

//fonction actualisation du menu deroulant 
export const updateDropDownMenus = (recipes) => {
    const allElementsMenu = [];
       
    for(let recipe of recipes) {               
        for(let ingredient of recipe.ingredients) {
           allElementsMenu.push(ingredient.ingredient);  
        }              
    }
    
    // Supprimer toutes les redondances
    const  withoutRedondanceIngredientItems = allElementsMenu.filter(
        (elt, pos) => {
        return allElementsMenu.indexOf(elt) == pos
    });

    return withoutRedondanceIngredientItems;      
}

export const ArraysComparison = (firstArray, secondArray) =>{
    return firstArray.filter(
        (elem1) => { 
        return !secondArray.some((elem2)=>  {return elem1 === elem2})     
    }) 
}

export const newIngredientDropDownFiltered = (arrayRecipes) => {
    const allElementsMenu = [];

    for(let recipe of arrayRecipes) {               
        for(let ingredient of recipe.ingredients) {
           allElementsMenu.push(ingredient.ingredient);  
        }              
    }
    return allElementsMenu;
}

export const newDeviceDropDownFiltered = (arrayRecipes) => {
    const allElementsMenu = [];
            
    for(let recipe of arrayRecipes) {                           
        allElementsMenu.push(recipe.appliance);                     
    }   
    return allElementsMenu;
}

export const newUtensilDropDownFiltered = (arrayRecipes) => {
    const allElementsMenu = [];

    for(let recipe of arrayRecipes) {               
        for(let utensil of recipe.ustensils) {
           allElementsMenu.push(utensil);  
        }              
    }
    return allElementsMenu;
}







