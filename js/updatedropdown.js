export const displayUpDateDropDownItems = (listMenus, differenceItems) => {

    for( let listMenu of listMenus){
        for(let item of differenceItems) {
            if(listMenu.textContent == item){
                listMenu.style.display = "none";
            }
        }
    }
}

export const displayInitialDropDownMenu = (listMenus) => {
    for( let listMenu of listMenus){                                    
        listMenu.style.display = "block";                                    
    }                                            
}


export const displayBackUpDateDropDownItems = (listMenus, differenceItems) => {

    for( let listMenu of listMenus){
        for(let item of differenceItems) {
            if((listMenu.textContent === item ) && (listMenu.style.display = "none")){
                listMenu.style.display = "block";
            }
        }
    }
}


