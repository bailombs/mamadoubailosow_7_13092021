
export const displayRecipes =  (containerRecipes, arrayRecipes) => {
        
    for(let recipe of arrayRecipes) {
        
        const containerRecipesItems = document.createElement('aside');
        containerRecipes.appendChild(containerRecipesItems);
        containerRecipesItems.classList.add('container-recipes__items');

        const recipePicture = document.createElement('aside');       
        const recipeDescription = document.createElement('aside');
        containerRecipesItems.appendChild(recipePicture);
        containerRecipesItems.appendChild(recipeDescription);

        recipePicture.classList.add('container-recipes__items--picture');
        recipeDescription.classList.add('container-recipes__items--description');

        const titleRecipe = document.createElement('p');
        const contentRecipe = document.createElement('aside');

        recipeDescription.appendChild(titleRecipe);
        recipeDescription.appendChild(contentRecipe);
        titleRecipe.classList.add('titleRecipe');
        titleRecipe.innerHTML = `<span class="titleRecipe__font">${recipe.name}</span><span class=" far fa-clock titleRecipe__icon"> ${recipe.time} min</span>` ;      
        contentRecipe.classList.add('contentrecipe');

        const leftContentRecipe = document.createElement('p');
        contentRecipe.appendChild(leftContentRecipe);
        leftContentRecipe.classList.add('contentrecipe__left');
        const rightContentRecipe = document.createElement('p');
        contentRecipe.appendChild(rightContentRecipe);
        rightContentRecipe.classList.add('contentrecipe__right');
        rightContentRecipe.innerHTML = `${recipe.description}` ;

        for(let ingredient of recipe.ingredients) {
           // Ingredients de chaque recette
            const ingredientContentRecipe = document.createElement('span');
            leftContentRecipe.appendChild(ingredientContentRecipe);
            ingredientContentRecipe.innerHTML =`${ingredient.ingredient}:`;
            ingredientContentRecipe.classList.add('contentrecipe__left--ingredient');
          
            // quantité ingredient de chaque recette 
            const quantityContentRecipe = document.createElement('span');
            if(ingredient.quantity !== undefined){
                leftContentRecipe.appendChild(quantityContentRecipe);
            }            
            quantityContentRecipe.innerHTML =` ${ingredient.quantity}`;

           // unité ingredient  de chaque recette 
            const unitContentRecipe = document.createElement('span');            
            if(ingredient.unit !== undefined){               
                leftContentRecipe.appendChild(unitContentRecipe);
            }
            unitContentRecipe.innerHTML = `${ingredient.unit}`;
            
            // retour à la  ligne à chaque tour de boucle
            const lineSaut = document.createElement('br');
            leftContentRecipe.appendChild(lineSaut);          
        }
         
    }
}