import { recipes } from "./recipes.js";
import { ArraysComparison, dropdownMenus, newDeviceDropDownFiltered, newIngredientDropDownFiltered, newUtensilDropDownFiltered} from "./dropdown.js";
import { displayRecipes } from "./recipe.js";
import { search1, search2 } from "./search.js";
import { setDropDownFiltersDevices, setDropDownFiltersIngredients, setDropDownFiltersUtensils, setFilterrecipes } from "./state.js";
import { displayInitialDropDownMenu, displayUpDateDropDownItems } from "./updateDropdown.js";


const recoverData = () => {     
    // DOM menu deroulant ingredients
    const containerIngredientMenu = document.querySelector('.ingredient');
    const ingredientBtnGroup = document.querySelector('.ingredient__toggle');
    const ingredientIcon = document.querySelector('.ingredient__toggle--icon');
    const ingredientMenu = document.querySelector('.ingredient__dropdownmenu');
    
   console.log(ingredientMenu);
    // DOM menu deroulant Appareils
    const containerDeviceMenu = document.querySelector('.device');
    const deviceBtnGroup = document.querySelector('.device__toggle');
    const deviceIcon = document.querySelector('.device__toggle--icon');
    const deviceMenu = document.querySelector('.device__dropdownmenu');
   

    // DOM menu deroulant ustensiles
    const containerUtensilMenu = document.querySelector('.utensil');
    const utensilBtnGroup = document.querySelector('.utensil__toggle');
    const utensilIcon = document.querySelector('.utensil__toggle--icon');
    const utensilMenu = document.querySelector('.utensil__dropdownmenu');

    
    //const tagsMenu = document.querySelector('.menus__tags');
    //DOM container des Recettes 
    const searchBar = document.querySelector('.header__ctner-search--input');
    const containerRecipes = document.querySelector('.container-recipes');
        

    //Evemenement listener de la barre de recherche
    
    searchBar.addEventListener('input', () => {    
        const contentSearch = searchBar.value.toLowerCase(); 
        const listMenusIngredients = ingredientMenu.querySelectorAll('li'); 
        const listMenusDevices = deviceMenu.querySelectorAll('li'); 
        const listMenusUtensil = utensilMenu.querySelectorAll('li'); 
        

        if(contentSearch.length <= 2) {
            containerRecipes.innerHTML = ""
            displayRecipes(containerRecipes, recipes); 
            containerRecipes.style.gridTemplateColumns = "repeat(3, 1fr)"; 
            displayInitialDropDownMenu(listMenusIngredients);
            displayInitialDropDownMenu(listMenusDevices);
            displayInitialDropDownMenu(listMenusUtensil);
            setFilterrecipes(recipes)
        }

           
        if(contentSearch.length < 3){
            return ;
        }    
      
        const recipesBySearch = search1(recipes, contentSearch)               
        //const recipesBySearch = search2(recipes, contentSearch)  
        setFilterrecipes(recipesBySearch)

        
        const recoverInitialIngredients = newIngredientDropDownFiltered(recipes)
        const recoverIngredientsAfterSearch = newIngredientDropDownFiltered(recipesBySearch); 
        
        const recoverInitialDevices = newDeviceDropDownFiltered(recipes)
        const recoverDevicesAfterSearch = newDeviceDropDownFiltered(recipesBySearch); 
        
        const recoverInitialUtensils =  newUtensilDropDownFiltered(recipes)
        const recoverUtensilsAfterSearch =  newUtensilDropDownFiltered(recipesBySearch); 


        const diffrenceIngredients =  ArraysComparison(recoverInitialIngredients, recoverIngredientsAfterSearch);
        const differenceDevices =  ArraysComparison(recoverInitialDevices, recoverDevicesAfterSearch);
        const differenceUtensils =  ArraysComparison(recoverInitialUtensils, recoverUtensilsAfterSearch);


        if(recipesBySearch.length === 0) {
            containerRecipes.innerHTML = "";
            const noResult = document.createElement('p');
            containerRecipes.appendChild(noResult);
            containerRecipes.style.gridTemplateColumns = "1fr";
            noResult.innerHTML = ` Aucun resultat ne correspond à votre recherche dans le champs principal. Veuillz essayer
            "Limonade de coco" ou "Tarte au thon" etc`;
            noResult.style.justifySelf = "center";
            noResult.style.fontSize = "1.5em";
            noResult.style.margin = "4rem";

           

            console.log(diffrenceIngredients);
           
            displayUpDateDropDownItems(listMenusIngredients, diffrenceIngredients)
            displayUpDateDropDownItems(listMenusDevices, differenceDevices)
            displayUpDateDropDownItems(listMenusUtensil, differenceUtensils)
            
           
            
        } else {
            containerRecipes.innerHTML = "";
            displayRecipes(containerRecipes, recipesBySearch); 
            containerRecipes.style.gridTemplateColumns = "repeat(3, 1fr)";          
    
            console.log(recipesBySearch);           
            console.log(recoverIngredientsAfterSearch, recoverDevicesAfterSearch, recoverUtensilsAfterSearch); 

            displayUpDateDropDownItems(listMenusIngredients, diffrenceIngredients)
            displayUpDateDropDownItems(listMenusDevices, differenceDevices)
            displayUpDateDropDownItems(listMenusUtensil, differenceUtensils)
            
            // Reperation des elements des menus deroulants après la recherche principale
            setDropDownFiltersIngredients(recoverIngredientsAfterSearch);
            setDropDownFiltersDevices(recoverDevicesAfterSearch);
            setDropDownFiltersUtensils(recoverUtensilsAfterSearch);
           
        }       
    })

    // Ajout dynamique de la liste des ingredients
    const ingredientItems = (recipes) => {
        const allElementsMenu = [];
       
        // recuperation de tous  les ingredients dans un nouveau tableau
        for(let recipe of recipes) {               
            for(let ingredient of recipe.ingredients) {
               allElementsMenu.push(ingredient.ingredient);  
            }              
        }
        
        // Supprimer toutes les redondances
        const  withoutRedondanceIngredientItems = allElementsMenu.filter(
            (elt, pos) => {
            return allElementsMenu.indexOf(elt) == pos
        });

       
        //affichage de tous les ingredients  sans redondance 
        for(let ingredient of withoutRedondanceIngredientItems) {                           
            allElementsMenu.push(ingredient.ingredient)
            const ingredientListItems = document.createElement('li');
            ingredientMenu.appendChild(ingredientListItems); 
            ingredientListItems.innerHTML = ingredient;  
            ingredientListItems.classList.add('ingredient__dropdownmenu--item');                         
        }
    }

    //Ajout dynamique de la liste des Appareils
    const deviceItems = (recipes) => {
        const allElementsMenu = [];
       
        // recuperation de tous  les Appareils dans un nouveau tableau
        for(let recipe of recipes) {                           
            allElementsMenu.push(recipe.appliance);                     
        }
        
        // Supprimer toutes les redondances
        const  withoutRedondanceDeviceItems = allElementsMenu.filter(
            (elt, pos) => {
            return allElementsMenu.indexOf(elt) == pos
        });

        //affichage de tous les appareils sans redondance 
        for(let device of withoutRedondanceDeviceItems ) {                   
            const ingredientListItems = document.createElement('li');
            deviceMenu.appendChild(ingredientListItems); 
            ingredientListItems.innerHTML = device ;
            ingredientListItems.classList.add('device__dropdownmenu--item');                               
        }
    }

    //Ajout dynamique de la liste des ustensiles
    const utensilItems = (recipes) => {
        const allElementsMenu = [];
       
        // recuperation de tous  les ingredients dans un nouveau tableau
        for(let recipe of recipes) {               
            for(let utensil of recipe.ustensils) {
               allElementsMenu.push(utensil);  
            }              
        }
        
        // Supprimer toutes les redondances
        const  withoutRedondanceUtensilItems = allElementsMenu.filter(
            (elt, pos) => {
            return allElementsMenu.indexOf(elt) == pos
        });

        //console.log(withoutRedondanceUtensilItems);
        //affichage de tous les ingredients  sans redondance 
        for(let utensil of withoutRedondanceUtensilItems) {                           
            const ingredientListItems = document.createElement('li');
            utensilMenu.appendChild(ingredientListItems); 
            ingredientListItems.innerHTML = utensil ;  
            ingredientListItems.classList.add('utensil__dropdownmenu--item');                                   
        }
        
    }


    //createTags(tagsMenu);
    // appel fonction interaction des 3 menus deroutants 
    dropdownMenus(containerIngredientMenu, ingredientBtnGroup, ingredientIcon, ingredientMenu, ingredientItems(recipes), 'Ingredients', 'Ingredient');
    dropdownMenus(containerDeviceMenu, deviceBtnGroup, deviceIcon, deviceMenu, deviceItems(recipes),'Appareil', 'Appareil');
    dropdownMenus(containerUtensilMenu, utensilBtnGroup, utensilIcon, utensilMenu, utensilItems(recipes), 'Ustensiles', 'Ustensile');

    // appel fonction affichages des recettes 
    displayRecipes(containerRecipes, recipes);

    
       
}

recoverData();










