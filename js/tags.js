import { newIngredientDropDownFiltered, newDeviceDropDownFiltered, newUtensilDropDownFiltered  } from "./dropdown.js";
import { displayRecipes } from "./recipe.js";
import { getFilterrecipes, setFilterrecipes } from "./state.js";
import { displayBackUpDateDropDownItems } from "./updateDropdown.js";

//fonction de creation d'un tags
export const createTags = (tagsMenu,tagName, color, dropdownMenu, listItemMenu, recipeBySearch) => {
    const containerRecipes = document.querySelector('.container-recipes');
   
    const dropdownMenuFilter = getFilterrecipes();
    
    const tagContainer = document.createElement('div');
    tagsMenu.appendChild(tagContainer);
    const tagButton = document.createElement('input');
    const tagIcon = document.createElement('i');

    tagContainer.appendChild(tagButton);
    tagContainer.appendChild(tagIcon);
    tagContainer.classList.add("menus__tags--tagcontainer");
    tagContainer.style.backgroundColor = color;
    tagButton.classList.add('tagbutton');
    tagButton.setAttribute("type","button");
    tagButton.setAttribute("value", tagName);
    tagIcon.classList.add("far");
    tagIcon.classList.add("fa-times-circle");
    tagIcon.classList.add("tagicon")

    //Dom Menus deroulants
    const listMenusIngredients = document.querySelectorAll('.ingredient__dropdownmenu--item'); 
    const listMenusDevices = document.querySelectorAll('.device__dropdownmenu--item'); 
    const listMenusUtensils = document.querySelectorAll('.utensil__dropdownmenu--item'); 
    
    tagIcon.addEventListener('click', ()=> {      
        tagsMenu.removeChild(tagContainer);
        dropdownMenu.appendChild(listItemMenu);
        containerRecipes.innerHTML = "";
        displayRecipes(containerRecipes, dropdownMenuFilter)
        containerRecipes.style.gridTemplateColumns = "repeat(3, 1fr)";  
        
        const recipesBySearchs = recipeBySearch
        console.log(recipesBySearchs);

        const ingredientsFiltered =   newIngredientDropDownFiltered(recipesBySearchs);               
        const devicesFiltered = newDeviceDropDownFiltered(recipesBySearchs)
        const utensilsFiltered =  newUtensilDropDownFiltered(recipesBySearchs)

        console.log(ingredientsFiltered, devicesFiltered, utensilsFiltered );
        console.log(ingredientsFiltered);
      
        if(tagsMenu.children.length  === 0 || recipesBySearchs.length >= 1 ) {          
            setFilterrecipes(recipesBySearchs);            
            containerRecipes.innerHTML = "";
            displayRecipes(containerRecipes, recipesBySearchs);  

            displayBackUpDateDropDownItems(listMenusIngredients, ingredientsFiltered );
            displayBackUpDateDropDownItems(listMenusDevices, devicesFiltered) 
            displayBackUpDateDropDownItems(listMenusUtensils, utensilsFiltered)                         
        }
       
    })
    
}
